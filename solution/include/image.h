//
// Created by morik on 07.12.2022.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H
#include  <stdint.h>
typedef struct image {
    uint64_t width, height;
    struct pixel* data;
} image;
void freeImage(image *image1);
#endif //IMAGE_TRANSFORMER_IMAGE_H
