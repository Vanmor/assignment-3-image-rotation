#include "image.h"
#include "read.h"
#include <stdio.h>

#ifndef IMAGE_TRANSFORMER_BMP_CREATE_HEADER_H
#define IMAGE_TRANSFORMER_BMP_CREATE_HEADER_H
bmp_header createHeader(image *img, bmp_header bmpHeader);
#endif //IMAGE_TRANSFORMER_BMP_CREATE_HEADER_H
