#include "read.h"
#include <malloc.h>



void saveBmpFile(bmp_header *bmpHeader, char *outFile, image image1) {

    FILE *out = fopen(outFile, "w+b");
    if (out) {
        bmp_header bmpWrite = createHeader(&image1, *bmpHeader);
        if (fwrite(&bmpWrite, sizeof(bmp_header), 1, out)) {
            fprintf(stderr, "%s", "error_bmp_write");
        }
        for (size_t i = 0; i < image1.height; i++) {
            if (fwrite(image1.data + i * image1.width, sizeof(struct pixel), image1.width, out) != image1.width) {
                fprintf(stderr, "%s", "error_image_write");
            }

            if (fseek(out, (long) getPadding(image1.width), SEEK_CUR) != 0) {
                fprintf(stderr, "%s", "error_image_seek");
            }
        }

        fclose(out);
    } else{
        fprintf(stderr, "%s", "file can't open");
        freeImage(&image1);
    }
}
