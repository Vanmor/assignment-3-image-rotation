#include "image.h"
#include "read.h"
#include <stdio.h>
#include <stdlib.h>





bmp_header *loadAndRotateBMPFile(char *fname, char *outFile) {
    FILE *file = fopen(fname, "rb");
    if (!file) {
        fprintf(stderr, "Can't load file \'%s\'\n", fname);
        exit(0);
    }

    bmp_header *bmpHeader = (bmp_header *) malloc(sizeof(bmp_header)); // выделение памяти под файл
    if (bmpHeader == NULL) {
        fprintf(stderr,"%s", "Can't load data");
        exit(0);
    }
    fread(bmpHeader, sizeof(bmp_header), 1, file); // считывание файла

    image image1;
    image1.width = bmpHeader->biWidth;
    image1.height = bmpHeader->biHeight;
    image1.data = malloc(image1.width * image1.height * sizeof(struct pixel)); // выделения памяти под массив с пикселями
    if (image1.data == NULL) {
        fprintf(stderr,"%s", "Can't load data");
        exit(0);
    }

    uint8_t pad = getPadding(image1.width);

    for (size_t i = 0; i < image1.height; i++) {
        if (fread(image1.data + (i * image1.width), sizeof(struct pixel), image1.width, file) != image1.width) {
            fprintf(stderr,"%s", "error_read");
            free(image1.data);
            exit(0);
        }

        if (fseek(file, pad, SEEK_CUR) != 0) {
            fprintf(stderr, "%s", "error_seek");
            exit(0);
        }
    }
    image transform;
    transform.data = malloc(image1.height * image1.width * sizeof(struct pixel));
    transform.width = image1.width;
    transform.height = image1.height;
    for (size_t i = 0; i < image1.height; ++i) {
        for (size_t j = 0; j < image1.width; ++j) {
            transform.data[image1.height * j + image1.height - i - 1] = image1.data[image1.width * i + j];
        }
    }
    image imageOut;
    imageOut.height = image1.width, imageOut.width = image1.height, imageOut.data = transform.data;

//    image1 = rotate(image1);
    fclose(file);

    saveBmpFile(bmpHeader, outFile, imageOut);
    freeImage(&transform);
    free(image1.data);
    return bmpHeader;

}

void freeBMPFile(bmp_header *bmp_file) {
    if (bmp_file) {
        free(bmp_file);
        bmp_file = NULL;
    }
}
