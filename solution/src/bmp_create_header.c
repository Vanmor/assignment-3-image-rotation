#include "image.h"
#include "read.h"
#include <stdio.h>

bmp_header createHeader(image *img, bmp_header bmpHeader) {
    size_t sizeImage = img->height * img->width * sizeof(struct pixel);
    return (struct bmp_header) {.bfType=bmpHeader.bfType,
            .bfReserved=bmpHeader.bfReserved,
            .bOffBits=bmpHeader.bOffBits,
            .biPlanes=bmpHeader.biPlanes,
            .biCompression=bmpHeader.biCompression,
            .biXPelsPerMeter=bmpHeader.biXPelsPerMeter,
            .biYPelsPerMeter=bmpHeader.biYPelsPerMeter,
            .biClrUsed=bmpHeader.biClrUsed,
            .biClrImportant=bmpHeader.biClrImportant,
            .biBitCount=bmpHeader.biBitCount,
            .biHeight=img->height,
            .biWidth=img->width,
            .biSizeImage =sizeImage,
            .bfileSize= bmpHeader.bOffBits + sizeImage,
            .biSize=bmpHeader.biSize};
}
