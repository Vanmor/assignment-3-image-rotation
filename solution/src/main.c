#include "read.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc != 3) {
        fprintf(stderr, "image_transformer takes exactly 2 parameters.\n");
        return 0;
    }
    bmp_header *bmpFile = loadAndRotateBMPFile(argv[1], argv[2]);
    freeBMPFile(bmpFile);
    return 0;
}
