#include "read.h"
#include "image.h"
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>

uint64_t getPadding(uint32_t width) {
    unsigned long long rowPadding = 4 - (width * sizeof(struct pixel)) % 4;
    return rowPadding;
}

image createImage(bmp_header *bmpHeader){
    image image1;
    int countColumns = 8;
    unsigned int data_size = bmpHeader->biWidth * bmpHeader->biHeight * bmpHeader->biBitCount / countColumns; // для выделения памяти по массив пикселей
    image1.width = bmpHeader->biWidth;
    image1.height = bmpHeader->biHeight;
    image1.data = (struct pixel *) (unsigned char *) malloc(data_size); // выделения памяти под массив с пикселями
    return image1;
}


void freeImage(image *image1) {
    free(image1->data);
}
